<?php

namespace App\Entity\Listener\Lifecycle;

use App\File\Upload\FileUploaderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Post;

class UploadPostImageListener
{
    private $uploader;

    public function __construct(FileUploaderInterface $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(Post $post)
    {
        $this->uploadPostImage($post);
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(Post $post)
    {
        $this->uploadPostImage($post);
    }

    private function uploadPostImage(Post $post)
    {
        $file = $post->getImage();

        if ($file instanceof UploadedFile) {
            $fileName = $this->uploader->upload($file);
            $post->setImage($fileName);
        }
    }
}
