<?php

namespace App\Entity\Listener\Lifecycle;

use App\Entity\AuthorAwareInterface;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

// TODO: Create & type hint RequestContext class.
class SetAuthorListener
{
    private $ts;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->ts = $tokenStorage;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(AuthorAwareInterface $authorAware)
    {
        if (null !== $currentUser = $this->getCurrentUser()) {
            $authorAware->setAuthor($currentUser);
        }
    }

    private function getCurrentUser()
    {
        if (!null === $token = $this->ts->getToken()) {
            return null;
        }

        if (!($user = $token->getUser()) instanceof User) {
            return null;
        }

        return $user;
    }
}
