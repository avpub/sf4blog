<?php

namespace App\Entity\Listener\Lifecycle;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User;

class EncodeUserPasswordListener
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist(User $user)
    {
        $this->encode($user);
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate(User $user)
    {
        $this->encode($user);

        $om = $args->getObjectManager();
        $uow = $om->getUnitOfWork();
        $meta = $om->getClassMetadata(User::class);

        $uow->recomputeSingleEntityChangeSet($meta, $user);
    }

    private function encode(User $user)
    {
        if (null === $plainPassword = $user->getPlainPassword()) {
            return;
        }

        $encoded = $this->passwordEncoder->encodePassword(
            $user,
            $plainPassword
        );

        $user->setPassword($encoded);
    }
}
