<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostCommentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class PostComment implements AuthorAwareInterface
{
    use AuthorAwareTrait;
    use CommentTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Post")
     */
    private $post;

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }
}
