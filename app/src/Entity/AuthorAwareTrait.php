<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait AuthorAwareTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $author;

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): AuthorAwareInterface
    {
        $this->author = $author;

        return $this;
    }
}
