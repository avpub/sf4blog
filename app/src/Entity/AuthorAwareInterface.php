<?php

namespace App\Entity;

interface AuthorAwareInterface
{
    public function getAuthor(): ?User;
    public function setAuthor(?User $author): self;
}
