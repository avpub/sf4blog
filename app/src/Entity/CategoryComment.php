<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryCommentRepository")
 * @ORM\EntityListeners({
 *      "App\Entity\Listener\Lifecycle\SetAuthorListener",
 * })
 * @ORM\HasLifecycleCallbacks
 */
class CategoryComment implements AuthorAwareInterface
{
    use CommentTrait;
    use AuthorAwareTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $category;

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
