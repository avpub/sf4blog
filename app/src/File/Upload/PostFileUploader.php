<?php

namespace App\File\Upload;

class PostFileUploader extends AbstractFileUploader
{
    public function getTargetDir(): string
    {
        return parent::getTargetDir().'/posts';
    }
}
