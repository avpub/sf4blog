<?php

namespace App\File\Upload;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileUploaderInterface
{
    public function upload(UploadedFile $file): string;
    public function getTargetDir(): string;
}
