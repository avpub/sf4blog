<?php

namespace App\File\Upload;

use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class AbstractFileUploader implements FileUploaderInterface
{
    protected $rootDir;
    protected $targetDir;

    public function __construct(string $rootDir, string $targetDir)
    {
        $this->targetDir = $targetDir;
        $this->rootDir = $rootDir;
    }

    public function upload(UploadedFile $file): string
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->rootDir.'/'.$this->getTargetDir(), $fileName);

        return $this->getTargetDir().'/'.$fileName;
    }

    public function getTargetDir(): string
    {
        return $this->targetDir;
    }
}
