<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin
            ->setEmail('admin@test.test')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
        ;
        $adminPass = $this->encoder->encodePassword($admin, 'admin');
        $admin->setPassword($adminPass);

        $manager->persist($admin);

        $user = new User();
        $user
            ->setEmail('test@test.test')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
        ;
        $userPass = $this->encoder->encodePassword($user, 'test');
        $user->setPassword($userPass);

        $manager->persist($user);

        $manager->flush();
    }
}
