<?php

namespace App\Form\Handler;

use App\Form\CategoryCommentType;

class CategoryCommentFormHandler extends AbstractFormHandler
{
    public function getType(): string
    {
        return CategoryCommentType::class;
    }
}
