<?php

namespace App\Form\Handler;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use App\Form\Exception\InvalidFormException;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractFormHandler
{
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    public function handle($entity, Request $request, array $options = [])
    {
        $form = $this->createForm($entity, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !$form->isValid()) {
            throw new InvalidFormException($form);
        }

        return $entity;
    }

    public function createForm($entity, array $options = []): FormInterface
    {
        return $this->formFactory->create($this->getType(), $entity, $options);
    }

    abstract public function getType(): string;
}
