<?php

namespace App\Form\Handler;

use App\Form\CategoryType;

class CategoryFormHandler extends AbstractFormHandler
{
    public function getType(): string
    {
        return CategoryType::class;
    }
}
