<?php

namespace App\Form\Handler;

use App\Form\PostType;

class PostFormHandler extends AbstractFormHandler
{
    public function getType(): string
    {
        return PostType::class;
    }
}
