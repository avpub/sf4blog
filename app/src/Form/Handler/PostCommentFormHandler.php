<?php

namespace App\Form\Handler;

use App\Form\PostCommentType;

class PostCommentFormHandler extends AbstractFormHandler
{
    public function getType(): string
    {
        return PostCommentType::class;
    }
}
