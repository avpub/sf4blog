<?php

namespace App\Request;

use App\Repository\RequestLogRepository;

class RequestInfo
{
    private $logRepo;

    public function __construct(RequestLogRepository $repo)
    {
        $this->logRepo = $repo;
    }

    public function browserStats()
    {
        return $this->logRepo->statsByUserAgent();
    }
}
