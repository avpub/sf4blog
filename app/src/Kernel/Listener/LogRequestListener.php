<?php

namespace App\Kernel\Listener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use App\Entity\User;
use App\Entity\RequestLog;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class LogRequestListener
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onKernelTerminate(PostResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        $clientIp = $request->getClientIp();
        $userAgent = $request->headers->get('User-Agent');

        $log = (new RequestLog())
            ->setClientIp($clientIp)
            ->setUserAgent($userAgent)
        ;

        $this->em->persist($log);
        $this->em->flush($log);
    }
}
