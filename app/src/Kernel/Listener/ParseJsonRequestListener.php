<?php

namespace App\Kernel\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ParseJsonRequestListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            if (!is_array($data)) {
                $data = [];
            }
            $current = $request->request->all();

            $request->request->replace(array_merge($current, $data));
        }
    }
}
