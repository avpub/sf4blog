<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    use PaginatorTrait;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function getPage(int $page = 1, $limit = 20): array
    {
        $q = $this->createQueryBuilder('p')
            ->addOrderBy('p.createdAt', 'DESC')
            ->getQuery();

        $paginator = $this->paginate($q, $page, $limit);
        $count = $paginator->count();

        return [
            'posts' => $paginator->getQuery()->getResult(),
            'count' => floor(($count) / $limit) + 1,
            'current_page' => $page,
        ];
    }

    /**
     * @return Post[]
     */
    public function findPostsForCategory(int $categoryId): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.category = :category_id')
            ->setParameter('category_id', $categoryId)
            ->getQuery()
            ->getResult()
        ;
    }
}
