<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    use PaginatorTrait;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getPage(int $page = 1, $limit = 20): array
    {
        $q = $this->createQueryBuilder('c')
            ->addOrderBy('c.id', 'DESC')
            ->getQuery();

        $paginator = $this->paginate($q, $page, $limit);
        $count = $paginator->count();

        return [
            'categories' => $paginator->getQuery()->getResult(),
            'count' => floor(($count) / $limit) + 1,
            'current_page' => $page,
        ];
    }
}
