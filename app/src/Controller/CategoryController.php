<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\Handler\CategoryFormHandler;
use App\Form\Exception\InvalidFormException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Repository\PostRepository;

class CategoryController extends Controller
{
    private $categoryRepository;
    private $postRepository;
    private $formHandler;
    private $em;

    public function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository,
        CategoryFormHandler $formHandler,
        EntityManagerInterface $em
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->formHandler = $formHandler;
        $this->em = $em;
    }

    /**
     * @Route("/categories", methods={"GET"}, name="categories_list")
     */
    public function list(Request $request): Response
    {
        $page = $this->categoryRepository->getPage($request->query->get('page', 1));

        return $this->render('category/list.html.twig', $page);
    }

    /**
     * @Route("/categories/new", methods={"GET"}, name="categories_create_form")
     */
    public function getCreateForm(Request $request): Response
    {
        $form = $this->formHandler->createForm(new Category());

        return $this->render('category/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route(
     *     "/categories/{category}",
     *     requirements={"category": "\d+"},
     *     methods={"GET"},
     *     name="categories_view"
     * )
     */
    public function view(Category $category)
    {
        return $this->render('category/view.html.twig', [
            'category' => $category,
            'posts' => $this->postRepository->findPostsForCategory($category->getId()),
        ]);
    }

    /**
     * @Route("/categories/new", methods={"POST"}, name="categories_create")
     */
    public function create(Request $request)
    {
        try {
            $category = $this->formHandler->handle(new Category(), $request);
        } catch (InvalidFormException $exception) {
            return $this->render('category/form.html.twig', ['form' => $exception->getForm()->createView()]);
        }

        $this->em->persist($category);
        $this->em->flush($category);

        return $this->redirectToRoute('categories_list');
    }

    /**
     * @Route("/categories/{category}/edit", methods={"GET"}, name="categories_update_form")
     */
    public function getUpdateForm(Category $category): Response
    {
        $form = $this->formHandler->createForm($category);

        return $this->render('category/form.html.twig', ['form' => $form->createView(), 'category' => $category]);
    }

    /**
     * @Route("/categories/{category}/edit", methods={"POST"}, name="categories_update")
     */
    public function update(Request $request, Category $category): Response
    {
        try {
            $category = $this->formHandler->handle($category, $request);
        } catch (InvalidFormException $exception) {
            return $this->render('category/form.html.twig', [
                'form' => $exception->getForm()->createView(),
                'category' => $category,
            ]);
        }

        $this->em->flush($category);

        return $this->redirectToRoute('categories_list');
    }

    /**
     * @Route("/categories/{category}/remove", methods={"POST"}, name="categories_remove")
     */
    public function remove(Category $category): Response
    {
        $this->em->remove($category);
        $this->em->flush();

        return $this->redirectToRoute('categories_list');
    }
}
