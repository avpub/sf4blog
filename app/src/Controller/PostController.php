<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\Handler\PostFormHandler;
use App\Form\Exception\InvalidFormException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PostController extends Controller
{
    private $postRepository;
    private $formHandler;
    private $em;

    public function __construct(PostRepository $postRepository, PostFormHandler $formHandler, EntityManagerInterface $em)
    {
        $this->postRepository = $postRepository;
        $this->formHandler = $formHandler;
        $this->em = $em;
    }

    /**
     * @Route("/posts", methods={"GET"}, name="posts_list")
     */
    public function list(Request $request): Response
    {
        $page = $this->postRepository->getPage($request->query->get('page', 1));

        return $this->render('post/list.html.twig', $page);
    }

    /**
     * @Route("/posts/{post}", requirements={"post": "\d+"}, methods={"GET"}, name="posts_view")
     */
    public function view(Post $post)
    {
        return $this->render('post/view.html.twig', ['post' => $post]);
    }

    /**
     * @Route("/posts/new", methods={"GET"}, name="posts_create_form")
     */
    public function getCreateForm(Request $request): Response
    {
        $form = $this->formHandler->createForm(new Post());

        return $this->render('post/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/posts/new", methods={"POST"}, name="posts_create")
     */
    public function create(Request $request)
    {
        try {
            $post = $this->formHandler->handle(new Post(), $request);
        } catch (InvalidFormException $exception) {
            return $this->render('post/form.html.twig', ['form' => $exception->getForm()->createView()]);
        }

        $this->em->persist($post);
        $this->em->flush($post);

        return $this->redirectToRoute('categories_view', ['category' => $post->getCategory()->getId()]);
    }

    /**
     * @Route("/posts/{post}/edit", methods={"GET"}, name="posts_update_form")
     */
    public function getUpdateForm(Post $post): Response
    {
        $form = $this->formHandler->createForm($post);

        return $this->render('post/form.html.twig', ['form' => $form->createView(), 'post' => $post]);
    }

    /**
     * @Route("/posts/{post}/edit", requirements={"post": "\d+"}, methods={"POST"}, name="posts_update")
     */
    public function update(Request $request, Post $post): Response
    {
        try {
            $post = $this->formHandler->handle($post, $request);
        } catch (InvalidFormException $exception) {
            return $this->render('post/form.html.twig', [
                'form' => $exception->getForm()->createView(),
                'post' => $post,
            ]);
        }

        $this->em->flush($post);

        return $this->redirectToRoute('categories_view', ['category' => $post->getCategory()->getId()]);
    }

    /**
     * @Route("/posts/{post}/remove", methods={"POST"}, name="posts_remove")
     */
    public function remove(Post $post): Response
    {
        $categoryId = $post->getCategory()->getId();

        $this->em->remove($post);
        $this->em->flush();

        return $this->redirectToRoute('categories_view', ['category' => $categoryId]);
    }
}
