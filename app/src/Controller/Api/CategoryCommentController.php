<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\CategoryCommentRepository;
use App\Repository\CategoryRepository;
use App\Form\Exception\InvalidFormException;
use App\Entity\Category;
use App\Entity\CategoryComment;
use App\Form\Handler\CategoryCommentFormHandler;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManagerInterface;

class CategoryCommentController extends Controller
{
    use ApiControllerTrait;

    private $categoryCommentRepository;
    private $serializer;
    private $formHandler;
    private $em;

    public function __construct(
        CategoryCommentRepository $categoryCommentRepository,
        CategoryCommentFormHandler $formHandler,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ) {
        $this->categoryCommentRepository = $categoryCommentRepository;
        $this->formHandler = $formHandler;
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @Route("categories/{category}/comments", methods={"POST"}, name="api_category_comment_post")
     */
    public function post(Request $request, Category $category)
    {
        $request->request->set('category', $category->getId());

        try {
            $comment = $this->formHandler->handle(new CategoryComment(), $request);
        } catch (InvalidFormException $exception) {
            return $this->serializedJsonResponse($this->serializer, $exception->getForm(), 400);
        }

        $this->em->persist($comment);
        $this->em->flush($comment);

        return $this->serializedJsonResponse($this->serializer, $comment, 201);
    }

    /**
     * @Route("categories/{category}/comments", methods={"GET"}, name="api_category_comment_get_collection")
     */
    public function cget(Request $request, Category $category): Response
    {
        $comments = $this->categoryCommentRepository->findCommentsForCategoryId($category->getId());

        return $this->serializedJsonResponse($this->serializer, $comments, 200);
    }

    /**
     * @Route(
     *     "categories/{category}/comments/{comment}",
     *     methods={"DELETE"},
     *     name="api_category_comment_delete_comment"
     * )
     */
    public function remove(Category $category, CategoryComment $comment): Response
    {
        $this->em->remove($comment);
        $this->em->flush();

        return new Response(null, 204);
    }
}
