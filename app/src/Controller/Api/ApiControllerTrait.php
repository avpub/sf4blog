<?php

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializerInterface;

trait ApiControllerTrait
{
    public function serializedJsonResponse(SerializerInterface $serializer, $data, int $statusCode, array $headers = []): Response
    {
        $headers = array_merge(['Content-Type' => 'application/json'], $headers);

        return new Response($serializer->serialize($data, 'json'), $statusCode, $headers);
    }
}
