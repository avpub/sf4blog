<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\PostCommentRepository;
use App\Repository\PostRepository;
use App\Form\Exception\InvalidFormException;
use App\Entity\Post;
use App\Entity\PostComment;
use App\Form\Handler\PostCommentFormHandler;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\EntityManagerInterface;

class PostCommentController extends Controller
{
    use ApiControllerTrait;

    private $postCommentRepository;
    private $serializer;
    private $formHandler;
    private $em;

    public function __construct(
        PostCommentRepository $postCommentRepository,
        PostCommentFormHandler $formHandler,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    ) {
        $this->postCommentRepository = $postCommentRepository;
        $this->formHandler = $formHandler;
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @Route("posts/{post}/comments", methods={"POST"}, name="api_post_comment_post")
     */
    public function post(Request $request, Post $post)
    {
        $request->request->set('post', $post->getId());

        try {
            $comment = $this->formHandler->handle(new PostComment(), $request);
        } catch (InvalidFormException $exception) {
            return $this->serializedJsonResponse($this->serializer, $exception->getForm(), 400);
        }

        $this->em->persist($comment);
        $this->em->flush($comment);

        return $this->serializedJsonResponse($this->serializer, $comment, 201);
    }

    /**
     * @Route("posts/{post}/comments", methods={"GET"}, name="api_post_comment_get_collection")
     */
    public function cget(Request $request, Post $post): Response
    {
        $comments = $this->postCommentRepository->findCommentsForPostId($post->getId());

        return $this->serializedJsonResponse($this->serializer, $comments, 200);
    }

    /**
     * @Route(
     *     "posts/{post}/comments/{comment}",
     *     methods={"DELETE"},
     *     name="api_post_comment_delete_comment"
     * )
     */
    public function remove(Post $post, PostComment $comment): Response
    {
        $this->em->remove($comment);
        $this->em->flush();

        return new Response(null, 204);
    }
}
