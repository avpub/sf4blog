<?php

namespace App\Tests\Controller;

use App\Tests\BaseFunctionalTest;

class SecurityControllerTest extends BaseFunctionalTest
{
    public function testLogin()
    {
        $this->client->followRedirects();
        $this->client->request(
            'POST',
            '/login',
            [
                'email' => 'admin@test.test',
                'password' => 'admin',
                '_csrf_token' => $this->generateCsrfToken('authenticate'),
            ]
        );

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }

    public function testLoginBadCredentials()
    {
        $this->client->followRedirects();
        $crawler = $this->client->request(
            'POST',
            '/login',
            [
                'email' => 'admin@test.test',
                'password' => '12341234',
                '_csrf_token' => $this->generateCsrfToken('authenticate'),
            ]
        );


        $this->assertContains('Invalid credentials', $this->client->getResponse()->getContent());
    }

    public function testNoCsrfToken()
    {
        $this->client->followRedirects();
        $this->client->request(
            'POST',
            '/login',
            [
                'email' => 'admin@test.test',
                'password' => 'admin',
            ]
        );

        $this->assertContains('Invalid CSRF token', $this->client->getResponse()->getContent());
    }
}
