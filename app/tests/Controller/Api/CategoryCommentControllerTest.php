<?php

namespace App\Tests\Controller\Api;

use App\Tests\BaseFunctionalTest;
use App\Entity\Category;

class CategoryCommentControllerTest extends BaseFunctionalTest
{
    public function testCreateComment()
    {
        $path = '/api/categories/'.$this->getLastId(Category::class).'/comments';
        $this->client->request(
            'POST',
            $path,
            [
                'authorName' => 'Test Test',
                'content' => 'Test content',
            ],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $this->client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
    }

    public function testMissingContent()
    {
        $path = '/api/categories/'.$this->getLastId(Category::class).'/comments';
        $this->client->request(
            'POST',
            $path,
            ['authorName' => 'Test Test'],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));

        $content = json_decode($response->getContent(), true);
        $this->assertTrue(isset($content['children']['content']['errors']));
        $this->assertTrue(in_array('This value should not be blank.', $content['children']['content']['errors']));
    }

    public function testMissingAuthorName()
    {
        $path = '/api/categories/'.$this->getLastId(Category::class).'/comments';
        $this->client->request(
            'POST',
            $path,
            ['content' => 'Test Test'],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));

        $content = json_decode($response->getContent(), true);
        $this->assertTrue(isset($content['children']['authorName']['errors']));
        $this->assertTrue(in_array('This value should not be blank.', $content['children']['authorName']['errors']));
    }

    public function testAuthorNameInvalid()
    {
        $path = '/api/categories/'.$this->getLastId(Category::class).'/comments';
        $this->client->request(
            'POST',
            $path,
            ['content' => 'Test Test', 'authorName' => 'Test'],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));

        $content = json_decode($response->getContent(), true);
        $this->assertTrue(isset($content['children']['authorName']['errors']));
    }
}
