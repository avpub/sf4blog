<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

abstract class BaseFunctionalTest extends WebTestCase
{
    protected $client;

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->client = null;
    }

    protected function generateCsrfToken($intention)
    {
        return $this->client->getContainer()->get('security.csrf.token_manager')->getToken($intention);
    }

    protected function getLastId(string $class): ?int
    {
        return $this
            ->client
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository($class)
            ->createQueryBuilder('e')
            ->select('MAX(e.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
