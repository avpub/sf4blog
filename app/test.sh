#!/bin/sh
php bin/console cache:clear --env=test && \
    php bin/console doctrine:migrations:migrate --env=test --no-interaction && \
    php bin/console doctrine:fixtures:load --ansi --no-interaction --env=test && \
    php -d memory_limit=-1 bin/phpunit
