require('../css/app.css');
require('../../vendor/kevinpapst/adminlte-bundle/Resources/assets/admin-lte');

import Axios from 'axios';
import Vue from 'vue';
import CommentsList from './components/comments/CommentsList.vue';
import FormError from './components/form/FormError.vue';
import Validator from 'vee-validate';

Vue.use(Validator);

Vue.component('comments-list', CommentsList);
Vue.component('form-error', FormError);

window.axios = Axios;

const app = new Vue({
    el: '#app'
});
