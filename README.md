Setup
=====
```
composer install
yarn install
```

Run migrations

```
bin/console doc:migrations:migrate
```

Build assets

```
bin/console assets:install
bin/console ckeditor:install public/ckeditor
yarn encore production
```

Create user
===========
`bin/console app:create-user admin@test.test test ROLE_ADMIN`

Pages
=====
`/admin`, `/admin/categories/*`, `/admin/posts/*` - only for logged in users with role `ROLE_ADMIN`

`/categories` - List categories

`/posts` - List posts

Running tests
=============
Before runnig tests make sure that:

* `TEST_DATABASE_URL` is set
* Test database exists (if not run `bin/console doc:database:create --env=test`)

To start the tests execute `./tests.sh`
